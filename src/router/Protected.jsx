import React from 'react';
import { Redirect, Route } from 'wouter';

function ProtectedRoute({ component: Component, ...restOfProps }) {
  const isAuthenticated = localStorage.getItem('isAuthenticated');

  return isAuthenticated ? (
    <Route {...restOfProps} component={Component} />
  ) : (
    <Redirect to="/admin/login" />
  );
}

export default ProtectedRoute;
