import { Route, Switch } from 'wouter';
import Album from '../pages/Album';
import Home from '../pages/Home';
import Policy from '../pages/Policy';
import Register from '../pages/Register';
import UserTerms from '../pages/UserTerms';

const Routes = () => (
  <Switch>
    <Route path="/user-terms" component={UserTerms} />
    <Route path="/privacy-policy" component={Policy} />
    <Route path="/shared/:code">
      {(params) => <Album code={params.code} />}
    </Route>
    <Route path="/register/:code">
      {(params) => <Register code={params.code} />}
    </Route>
    <Route path="/" component={Home} />
    <Route>Page Not Found</Route>
  </Switch>
);

export default Routes;
