import React from 'react';
import ReactDOM from 'react-dom/client';
import Routes from './router/index.jsx';
import './styles/app.css';
import './styles/theme.scss';

ReactDOM.createRoot(document.getElementById('root')).render(<Routes />);
