import React from 'react';
import banner from '../assets/banner2.png';
import galleryImage from '../assets/gallery.png';
import qrcodeImage from '../assets/qrcode.png';
import syncImage from '../assets/sync.png';
import Features from '../components/Features';
import Footer from '../components/Footer';
import NavBar from '../components/Navbar';
import Signup from '../components/Signup';

export default function Home() {
  const [showModel, setShowModel] = React.useState(false);

  const toggleModel = () => {
    setShowModel(!showModel);
  };

  return (
    <div className="box">
      <NavBar />
      <div
        className="container is-flex is-justify-content-space-around is-align-content-center is-flex-wrap-wrap"

      >
        <section className="hero">
          <div className="hero-body">
            <span className="tag is-dark">Photographers</span>
            <span className="tag is-dark mx-3 mb-5">Event Managers</span>
            <p
              className="title"
              style={{ fontSize: '3.2em', fontFamily: 'Libre Franklin' }}
            >
              Show Your Story
            </p>
            <p
              className="subtitle"
              style={{ fontSize: '2em', fontFamily: 'Libre Franklin' }}
            >
              Instantly with others
            </p>
            <a className="button is-primary" onClick={() => toggleModel()}>
              <strong>Sign Up Now</strong>
            </a>
          </div>
        </section>
        <img src={banner} className="image" width={400}></img>
      </div>
      <div className="container is-flex is-justify-content-space-around is-align-content-center is-flex-wrap-wrap">
        <h3 className="subtitle is-3">How it works?</h3>
        <div className="columns">
          <div className="column">
            <div
              className="card m-3"
              style={{
                background: '#f5f5f5',
                boxShadow: 'none',
                border: '3px dashed #d5d5d8',
                borderRadius: '25px',
              }}
            >
              <div className="card-content">
                <div className="content is-justify-content-center is-flex is-flex-direction-column is-align-items-center">
                  <h3 className="subtitle is-4 has-text-centered">
                    Sync Images
                  </h3>
                  <p className="subtitle  is-size-6 has-text-centered">
                    Create an event and sync your images with us!
                  </p>
                  <figure className="image is-256x256">
                    <img src={syncImage} />
                  </figure>
                </div>
              </div>
            </div>
          </div>
          <div className="column">
            <div
              className="card m-3"
              style={{
                background: '#f5f5f5',
                boxShadow: 'none',
                border: '3px dashed #d5d5d8',
                borderRadius: '25px',
              }}
            >
              <div className="card-content">
                <div className="content is-justify-content-center is-flex is-flex-direction-column is-align-items-center">
                  <h3 className="subtitle is-4 has-text-centered">
                    Share QR Code
                  </h3>
                  <p className="subtitle  is-size-6 has-text-centered">
                    We generate QR Code for you to share with event guests.
                  </p>
                  <figure className="image is-256x256">
                    <img src={qrcodeImage} />
                  </figure>
                </div>
              </div>
            </div>
          </div>
          <div className="column">
            <div
              className="card m-3"
              style={{
                background: '#f5f5f5',
                boxShadow: 'none',
                border: '3px dashed #d5d5d8',
                borderRadius: '25px',
              }}
            >
              <div className="card-content">
                <div className="content is-justify-content-center is-flex is-flex-direction-column is-align-items-center">
                  <h3 className="subtitle is-4 has-text-centered">
                    Gallery on Phone
                  </h3>
                  <p className="subtitle  is-size-6 has-text-centered">
                    Guests can scan QR code and view images on their phone.
                  </p>
                  <figure className="image is-256x256">
                    <img src={galleryImage} />
                  </figure>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
      <div className="container my-5">
        <h3 className="subtitle is-3 has-text-centered">Features</h3>
        <Features />
      </div>

      <div className="container my-5 pt-5">
        <h3 className="subtitle is-3 has-text-centered">Have any questions?</h3>
        <div
          className="content is-justify-content-center is-flex is-align-items-center"
          style={{
            height: 100,
          }}
        >
          <div className="field is-grouped">
            {/* <p className="control">
              <a
                className="button  is-success"
                href="https://wa.me/917305957605"
              >
                <span className="icon-text">
                  <span className="icon">
                    <ion-icon name="logo-whatsapp"></ion-icon>
                  </span>
                  <span>Chat</span>
                </span>
              </a>
            </p> */}
            <p className="control">
              <a type='primary'
                className="button"
                href="mailto:contact@picturetube.live?subject=I have a question!"
              >
                <span className="icon-text">
                  <span className="icon">
                    <ion-icon name="mail-outline"></ion-icon>
                  </span>
                  <span>Email Us</span>
                </span>
              </a>
            </p>
            {/* <p className="control">
              <a className="button is-link " href="tel:+917305957605">
                <span className="icon-text">
                  <span className="icon">
                    <ion-icon name="call-outline"></ion-icon>
                  </span>
                  <span>Call Us</span>
                </span>
              </a>
            </p> */}
          </div>
        </div>
      </div>

      <Footer />
      {showModel && <Signup showModel={showModel} toggleModel={toggleModel} />}
    </div>
  );
}
