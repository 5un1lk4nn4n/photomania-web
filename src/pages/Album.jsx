import { nanoid } from 'nanoid';
import PropTypes from 'prop-types';
import React from 'react';
import Masonry, { ResponsiveMasonry } from 'react-responsive-masonry';
import banner from '../assets/banner3.png';
import emptyBanner from '../assets/empty.png';
import errBanner from '../assets/error.png';
import Footer from '../components/Footer';
import NavBar from '../components/Navbar';
const eventDetails = {};
// will remove later
export default function Album({ code }) {
  const [albums, setAlbums] = React.useState([]);
  const [eventErr, setEventErr] = React.useState();
  const [event, setEvent] = React.useState();
  const [fetching, setFetching] = React.useState(false);
  const [error, setError] = React.useState(false);
  const [isLoading, setIsIsLoading] = React.useState(false);
  React.useEffect(() => {
    fetch(`http://localhost:7000/api/events/album/${code}`)
      .then((r) => {
        if (r.ok) {
          return r.json();
        }
        return Promise.reject(r);
      })
      .then((json) => {
        setEvent(json);
        const url = 'http://localhost:8000/api/matched';
        const data = {
          selfie_image_url: json.selfie_url,
          event_code: json.code,
        };
        fetch(url, {
          method: 'POST',
          headers: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(data),
        })
          .then((r) => {
            if (r.ok) {
              return r.json();
            }
            return Promise.reject(r);
          })
          .then((res) => {
            eventDetails.eventCode = res.event_code;
            eventDetails.selfieImage = res.selfie_image;
            eventDetails.noOfImages = res.no_of_images;
            eventDetails.executionTime = res.execution_time;
            eventDetails.numberOfFacesDetectedInSelfie =
              res.face_detected_in_selfie;
            setAlbums(res.images);
            console.log("all ok: mathched");
          })
          .catch((r) => {
            if (r.status === 400) {
              r.json().then((json) => {
                setEventErr(json);
              });
            }
            if (r.status === 404) {
              setEventErr({
                code: 3,
                message:
                  'Gallery is not available yet! Please try later again! ',
              });
            }
          })
          .finally(() => setIsIsLoading(false));
      })
      .catch((r) => {
        if (r.status === 400) {
          r.json().then((json) => {
            setEventErr(json);
          });
        }
        if (r.status === 404) {
          console.log({
            s: r.status,
          });
          setEventErr({
            code: 3,
            message: 'URL is invalid',
          });
        }
      });
  }, []);
  const download = (url) => {
    const name = nanoid();
    if (!url) {
      throw new Error('Resource URL not provided! You need to provide one');
    }
    setFetching(true);
    fetch(url)
      .then((response) => response.blob())
      .then((blob) => {
        setFetching(false);
        const blobURL = URL.createObjectURL(blob);
        const a = document.createElement('a');
        a.href = blobURL;
        a.style = 'display: none';

        if (name && name.length) a.download = name;
        document.body.appendChild(a);
        a.click();
      })
      .catch(() => setError(true));
  };

  const getErrorDisplay = () => {
    return (
      <div
        className="is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-align-self-center"
        style={{
          minHeight: '90vh',
        }}
      >
        <h4 className="title is-5 has-text-centered">{eventErr?.message}</h4>
        <img src={errBanner} width={200} />
      </div>
    );
  };

  const getGallery = () => {
    if (albums.length > 0) {
      return (
        <div>
          <figure className="image is-64x64">
            <img className="is-rounded" src={eventDetails?.selfieImage} />
          </figure>
          <div>{eventDetails?.no_of_images}</div>

          <ResponsiveMasonry
            columnsCountBreakPoints={{ 350: 1, 750: 2, 900: 3 }}
          >
            <Masonry>
              {/* {albums.map((image, i) => (
          <img
            key={i}
            src={image}
            style={{ width: '90%', display: 'block' }}
          />
        ))} */}
              {albums &&
                albums.map((image, index) => {
                  return (
                    <div
                      key={index}
                      style={{
                        margin: 5,
                        borderRadius: 15,
                        position: 'relative',
                      }}
                    >
                      <img
                        src={image}
                        style={{
                          borderRadius: 10,
                        }}
                      />
                      <button
                        className="button  is-small"
                        onClick={() => download(image)}
                        style={{
                          position: 'absolute',
                          bottom: 15,
                          right: 10,
                        }}
                      >
                        <span className="icon ">
                          <ion-icon name="download-outline"></ion-icon>
                        </span>
                      </button>
                    </div>
                  );
                })}
            </Masonry>
          </ResponsiveMasonry>
        </div>
      );
    }

    return (
      <div className="is-flex is-justify-content-center is-align-items-center is-align-self-center is-flex-wrap-wrap p-3 is-flex-direction-column mb-5 ">
        <img
          src={emptyBanner}
          className="image"
          style={{
            width: '50%',
            // maxHeight: '100%',
          }}
        />
        <h5
          className="subtitle is-5"
          style={{
            fontFamily: 'Libre Franklin',
          }}
        >
          Gallery not available yet!
        </h5>
      </div>
    );
  };
  console.log({ eventDetails, isLoading });
  return (
    <div
      className="box"
      style={{
        minHeight: '100vh',
      }}
    >
      <NavBar />

      {eventErr ? (
        getErrorDisplay()
      ) : (
        <>
          {' '}
          <div
            style={{
              position: 'absolute',
              boxShadow: 'none',
              height: 50,
              background: 'tomato',
              zIndex: 1,
              top: '10px',
              left: '10px',
              bottom: '10px',
            }}
          ></div>
          <div
            className="is-flex is-justify-content-center is-align-items-center is-align-self-center is-flex-wrap-wrap p-3 "
            style={{
              background: '##cdcdc8',
              // height: 300,
            }}
          >
            <img src={event?.poster ? event?.poster : banner} width={200} />
            <div className="content has-text-centered">
              <h5
                className="subtitle is-5"
                style={{
                  fontFamily: 'Libre Franklin',
                }}
              >
                Welcome to <strong>{event?.name}</strong>!
              </h5>
              <p
                style={{
                  fontFamily: 'Libre Franklin',
                }}
              >
                Memory collection made for you!
              </p>
            </div>
          </div>
          {isLoading ? <p>Loading...</p> : getGallery()}
        </>
      )}

      <Footer />
    </div>
  );
}

Album.propTypes = {
  code: PropTypes.string.isRequired,
};
