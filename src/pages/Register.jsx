import { nanoid } from 'nanoid';
import PropTypes from 'prop-types';
import React from 'react';
import Camera, { IMAGE_TYPES } from 'react-html5-camera-photo';
import 'react-html5-camera-photo/build/css/index.css';
import { Redirect } from 'wouter';
import banner from '../assets/banner3.png';
import errBanner from '../assets/error.png';
import Footer from '../components/Footer';
import NavBar from '../components/Navbar';
const rediretPrams = {};
export default function Register({ code }) {
  const [errors, setErrors] = React.useState({ mobile: false });
  const [dataUri, setDataUri] = React.useState('');
  const [mobile, setMobile] = React.useState('');
  const [aggred, setAggred] = React.useState(true);
  const [isRegistering, setIsRegistering] = React.useState(false);
  const [err, setErr] = React.useState();
  const [event, setEvent] = React.useState();
  const [eventErr, setEventErr] = React.useState();
  const [isRegisterSuccessful, setIsRegisterSuccessful] = React.useState(false);
  const isFullscreen = false;

  React.useEffect(() => {
    fetch(`http://localhost:7000/api/events/qr-code/${code}`)
      .then((r) => {
        if (r.ok) {
          return r.json();
        }
        return Promise.reject(r);
      })
      .then((json) => {
        setEvent(json);
      })
      .catch((r) => {
        if (r.status === 400) {
          r.json().then((json) => {
            setEventErr(json);
          });
        }
        if (r.status === 404) {
          setEventErr({
            code: 3,
            message: 'QR Code is invalid',
          });
        }
      });
  }, []);

  function dataURItoBlob(dataURI) {
    let byteString = atob(dataURI.split(',')[1]);

    // separate out the mime component
    let mimeString = dataURI.split(',')[0].split(':')[1].split(';')[0];

    let ab = new ArrayBuffer(byteString.length);
    let ia = new Uint8Array(ab);
    for (let i = 0; i < byteString.length; i++) {
      ia[i] = byteString.charCodeAt(i);
    }
    let blob = new Blob([ab], { type: mimeString });
    return blob;
  }

  function padWithZeroNumber(number, width) {
    number = number + '';
    return number.length >= width
      ? number
      : new Array(width - number.length + 1).join('0') + number;
  }

  function getFileExtention(blobType) {
    // by default the extention is .png
    let extention = IMAGE_TYPES.PNG;

    if (blobType === 'image/jpeg') {
      extention = IMAGE_TYPES.JPG;
    }
    return extention;
  }

  function getFileName(imageNumber, blobType) {
    const prefix = 'photo';
    const photoNumber = padWithZeroNumber(imageNumber, 4);
    const extention = getFileExtention(blobType);

    return `${prefix}-${photoNumber}.${extention}`;
  }

  function downloadImageFile(dataUri) {
    let blob = dataURItoBlob(dataUri);
    // downloadImageFileFomBlob(blob, nanoid());
    window.URL = window.webkitURL || window.URL;
    const filename = nanoid();
    const name = getFileName(filename, blob.type);

    return [blob, name];
  }
  const onChangeMobile = (value) => {
    setMobile(value);
  };
  function handleTakePhotoAnimationDone(dataUri) {
    console.log('takePhoto');
    setDataUri(dataUri);
  }

  const getErrorDisplay = () => {
    return (
      <div
        className="is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-align-self-center"
        style={{
          minHeight: '90vh',
        }}
      >
        <h4 className="title is-5 has-text-centered">{eventErr?.message}</h4>
        <img src={errBanner} width={200} />
      </div>
    );
  };

  const register = () => {
    if (!aggred) {
      return;
    }
    if (
      (mobile && mobile.length !== 10) ||
      !/^\d{10}$/.test(parseInt(mobile))
    ) {
      setErrors({
        mobile: true,
      });
      return;
    } else {
      setErrors({
        mobile: false,
      });
    }

    if (!dataUri) {
      setErrors({
        photo: true,
      });
      return;
    } else {
      setErrors({
        photo: false,
      });
    }

    const [blob, filename] = downloadImageFile(dataUri);

    console.log({
      blob,
      filename,
    });

    setIsRegistering(true);

    let formData = new FormData();
    formData.append('file', blob, filename);

    fetch('http://localhost:7000/api/file-manager/', {
      body: formData,
      method: 'post',
    })
      .then((r) => r.json())
      .then((r) => {
        fetch('http://localhost:7000/api/face/', {
          body: JSON.stringify({
            qr_code: code,
            mobile: mobile,
            face_image: r['file_id'],
          }),
          method: 'post',
          headers: {
            'Content-Type': 'application/json',
          },
        })
          .then((r) => {
            if (r.ok) {
              return r.json();
            }

            return Promise.reject(r);
          })
          .then((json) => {
            rediretPrams['code'] = json['code'];
            setIsRegisterSuccessful(true);
          })
          .catch((r) => {
            if (r.status === 400) {
              r.json().then((json) => {
                setErr(json.error);
              });
            }
          })
          .finally(() => {
            setIsRegistering(false);
          });
      });
  };

  if (isRegisterSuccessful) {
    return <Redirect to={`/shared/${rediretPrams?.code}`} />;
  }

  return (
    <div
      className="box "
      style={{
        minHeight: '100vh',
      }}
    >
      <NavBar />
      {eventErr ? (
        getErrorDisplay()
      ) : (
        <div className="is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-align-self-center mb-4">
          <div className="is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-align-self-center">
            <img src={event?.poster ? event?.poster : banner} width={300} />
            <div className="content has-text-centered">
              <h5
                className="subtitle is-5"
                style={{
                  fontFamily: 'Libre Franklin',
                }}
              >
                Welcome to <strong>{event?.name}</strong>!
              </h5>
              <p>
                Register here with your selfie and we will provide you a URL
                with the photo gallery featuring you!
              </p>
            </div>
          </div>
          <div className="mt-5">
            <p>Mobile</p>
            <input
              onChange={(e) => onChangeMobile(e.target.value)}
              type="tel"
              className="input"
              placeholder="Eg: 9956001000"
              style={{
                width: '300px',
              }}
              value={mobile}
            />
            {errors?.mobile && (
              <p className="help is-danger">Mobile no is invalid</p>
            )}

            <p className="mt-4">Talk a Selfie</p>
            <div
              style={{
                width: 300,
                padding: 10,
                // height: 300,
                border: '2px solid #a75db4',
                borderRadius: 8,
              }}
            >
              {dataUri ? (
                <div className="is-flex is-flex-direction-column is-justify-content-center is-align-items-center is-align-self-center">
                  <img src={dataUri} />
                  <button
                    className="button mt-5"
                    onClick={() => setDataUri(null)}
                  >
                    <span className="icon is-small">
                      <ion-icon name="reload-outline"></ion-icon>
                    </span>
                  </button>
                </div>
              ) : (
                <Camera
                  onTakePhotoAnimationDone={handleTakePhotoAnimationDone}
                  isFullscreen={isFullscreen}
                />
              )}
            </div>
            {errors?.photo && (
              <p className="help is-danger">
                Please take a selfie with clear face
              </p>
            )}

            <label className="checkbox mt-4">
              <input
                type="checkbox"
                checked={aggred}
                onChange={(e) => setAggred(e.target.checked)}
              />
            </label>

            <span className="ml-3">
              I agree to the <a href="/user-terms">Picturetube Terms</a> and{' '}
              <a href="/privacy-policy">Privacy Policy</a>
            </span>
            {!aggred && (
              <p className="help is-danger">Please accept our aggreement</p>
            )}
          </div>
          <article className="message mt-4 ">
            <div className="message-body">
              Note: We will delete all information about you once the event is
              expired. To know more, see our{' '}
              <a href="/privacy-policy">Policy Policy</a>
            </div>
          </article>
          <button
            className={`button is-primary ${isRegistering && 'is-loading'}`}
            onClick={() => {
              register();
            }}
          >
            Register
          </button>
          {err && <p className="help is-danger">{err}</p>}
        </div>
      )}

      <Footer />
    </div>
  );
}

Register.propTypes = {
  code: PropTypes.string.isRequired,
};
