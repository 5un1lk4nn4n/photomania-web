import React from 'react';
export default function QrScanner({ showModel, toggleModel }) {
  const [signUp, setSignUp] = React.useState({});
  const [errors, setErrors] = React.useState({});
  const onChangeRegister = (key, value) => {
    setSignUp((prevState) => ({
      ...prevState,
      [key]: value,
    }));
  };

  const onSubmit = () => {
    var hasError;

    console.log({
      signUp,
      f: !Object.keys(signUp).length,
    });

    if (!Object.keys(signUp).length) {
      console.log('WTF');
      hasError = {
        mobile: true,
        company: true,
        terms: true,
      };
      setErrors(hasError);
      return;
    }

    console.log({
      signUp,
      f: !Object.keys(signUp).length,
    });

    hasError = Object.keys(signUp).reduce((error, key) => {
      if (key == 'mobile' && !/^\d{10}$/.test(signUp[key])) {
        error[key] = true;
      } else {
        error[key] = false;
      }

      if (key == 'company' && !signUp[key]) {
        error[key] = true;
      } else {
        error[key] = false;
      }

      if (key == 'terms' && !signUp[key]) {
        error[key] = true;
      } else {
        error[key] = false;
      }

      console.log({
        error,
      });

      return error;
    }, {});

    console.log({
      hasError,
    });

    if (Object.keys(hasError)) {
      setErrors(hasError);
      return;
    }

    alert('all ok');
  };
  return (
    <div className={showModel ? 'modal is-active' : 'model'}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Sign Up</p>
        </header>

        <section className="modal-card-body">
          <div>
            <p>Mobile</p>
            <input
              onChange={(e) => onChangeRegister('mobile', e.target.value)}
              type="tel"
              className="input"
              placeholder="Mobile Number"
            />
            {errors?.mobile && (
              <p className="help is-danger">Mobile No is invalid</p>
            )}

            <p className="mt-4">Company</p>
            <input
              onChange={(e) => onChangeRegister('mobile', e.target.value)}
              type="tel"
              className="input"
              placeholder="Company Name"
            />
            {errors?.company && (
              <p className="help is-danger">Company Name cannot be blank</p>
            )}

            <label className="checkbox mt-4">
              <input
                type="checkbox"
                onChange={(e) => onChangeRegister('mobile', e.target.checked)}
              />
            </label>
            <span className="ml-3">
              I agree to the <a href="/user-terms">Picturetube Terms</a> and{' '}
              <a href="/privacy-policy">Privacy Policy</a>
            </span>
            {errors?.terms && (
              <p className="help is-danger">
                Please tick above to agree our terms.
              </p>
            )}
          </div>
          <article className="message mt-4 ">
            <div className="message-body">
              Once Sign Up, Our Team will call and guide you on how to proceed
              further.
            </div>
          </article>
        </section>
        <footer className="modal-card-foot">
          <button className="button is-primary" onClick={() => onSubmit()}>
            Sign Up
          </button>
          <button className="button" onClick={() => toggleModel()}>
            Cancel
          </button>
        </footer>
      </div>
      <button
        className="modal-close is-large"
        aria-label="close"
        onClick={() => toggleModel()}
      ></button>
    </div>
  );
}
