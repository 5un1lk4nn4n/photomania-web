import PropTypes from 'prop-types';
import React from 'react';
export default function Signup({ showModel, toggleModel }) {
  const [signUp, setSignUp] = React.useState({});
  const [error, setError] = React.useState(null);
  const [isRegistering, setIsRegistering] = React.useState(false);
  const [isSuccess, setIsSuccess] = React.useState(false);
  const onChangeRegister = (key, value) => {
    setSignUp((prevState) => ({
      ...prevState,
      [key]: value,
    }));
  };

  const onSubmit = () => {
    const formFields = Object.keys(signUp);

    if (!formFields.length) {
      setError('Please fill all the details');
      return;
    }

    if (
      (signUp['mobile'] && signUp['mobile'].length !== 10) ||
      !/^\d{10}$/.test(parseInt(signUp['mobile']))
    ) {
      setError('Mobile number is blank or invalid');
      return;
    }

    if (!signUp['company']) {
      setError('Company name cannot be blank');
      return;
    }

    if (!signUp['terms']) {
      setError('Please agree to our User Terms');
      return;
    }
    setError(null);
    setIsRegistering(true);
    setIsSuccess(false);
    // https://backend.picturetube.live/api/signups/
    fetch('http://localhost:8000/api/signups/', {
      method: 'post',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ ...signUp }),
    })
      .then((r) => {
        if (!r.ok) {
          let err = new Error('Mobile number already registered with us!');
          err.r = r;
          err.status = r.status;
          throw err;
        }
        return r;
      })
      // .then((r) => r.json())
      .then(() => setIsSuccess(true))
      .catch((e) => {
        setError(e.message);
      })
      .finally(() => setIsRegistering(false));
  };
  return (
    <div className={showModel ? 'modal is-active' : 'model'}>
      <div className="modal-background"></div>
      <div className="modal-card">
        <header className="modal-card-head">
          <p className="modal-card-title">Thank you for signing up with us!</p>
        </header>

        <section className="modal-card-body">
          <div>
            <p>Mobile</p>
            <input
              onChange={(e) => onChangeRegister('mobile', e.target.value)}
              type="tel"
              className="input"
              placeholder="Mobile Number"
            />

            <p className="mt-4">Company</p>
            <input
              onChange={(e) => onChangeRegister('company', e.target.value)}
              type="text"
              className="input"
              placeholder="Company Name"
            />

            <label className="checkbox mt-4">
              <input
                type="checkbox"
                onChange={(e) => onChangeRegister('terms', e.target.checked)}
              />
            </label>
            <span className="ml-3">
              I agree to the <a href="/user-terms">Picturetube Terms</a> and{' '}
              <a href="/privacy-policy">Privacy Policy</a>
            </span>
          </div>
          <article className="message mt-4 is-info">
            <div className="message-body">
              Once Sign Up, Our Team will call and guide you on how to proceed
              further.
            </div>
          </article>
        </section>
        <footer className="modal-card-foot">
          <button
            className={`button is-primary ${isRegistering && 'is-loading'}`}
            onClick={() => onSubmit()}
          >
            Sign Up
          </button>
          <button
            className="button disabled"
            disabled={isRegistering}
            onClick={() => toggleModel()}
          >
            Cancel
          </button>
          {error && <p className="help is-danger">{error}</p>}
          {isSuccess && (
            <article className="has-background-success has-text-white-ter p-3">
              <div className="">Registration Successful</div>
            </article>
          )}
        </footer>
      </div>
      <button
        disabled={isRegistering}
        className="modal-close is-large"
        aria-label="close"
        onClick={() => toggleModel()}
      ></button>
    </div>
  );
}

Signup.propTypes = {
  showModel: PropTypes.bool,
  toggleModel: PropTypes.func,
};
