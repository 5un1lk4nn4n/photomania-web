import React from 'react';

import logo from '../assets/logo.svg'
import Signup from './Signup';
export default function NavBar() {
  const [showMenu, setShowMenu] = React.useState(false);
  const [showModel, setShowModel] = React.useState(false);

  const toggleModel = () => {
    setShowModel(!showModel);
  };

  const toggleShowMenu = () => {
    setShowMenu(!showMenu);
  };

  return (
    <>
      <nav
        className="navbar px-2"
        role="navigation"
        aria-label="main navigation"
        style={{
          position: 'sticky',
          top: 0,
          boxShadow: 'none',
        }}
      >
        <div className="navbar-brand">
          <a className="navbar-item" href="/" style={{
            textDecoration: "none"
          }}>
            <img src={logo} style={{ maxHeight: 80 }} />

          </a>
          <a
            onClick={toggleShowMenu}
            role="button"
            className={`navbar-burger ${showMenu && 'is-active'}`}
            aria-label="menu"
            aria-expanded="false"
            data-target="navbarMenus"
          >
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
            <span aria-hidden="true"></span>
          </a>
        </div>

        <div
          className={`navbar-menu ${showMenu && 'is-active'}`}
          id="navbarMenus"
        >
          <div className="navbar-end">
            <div className="navbar-item">
              <div className="buttons">
                <a className="button is-primary" onClick={() => toggleModel()}>
                  <strong>Sign Up</strong>
                </a>
                <a className="button is-light" href="https://me.picturetube.live">
                  Log in
                </a>
              </div>
            </div>
          </div>
        </div>
      </nav>
      {showModel && <Signup showModel={showModel} toggleModel={toggleModel} />}
    </>
  );
}
