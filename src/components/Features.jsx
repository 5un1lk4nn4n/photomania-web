export default function Feature() {
  return (
    <div className="tile is-ancestor">
      <div className="tile is-vertical is-8">
        <div className="tile">
          <div className="tile is-parent is-vertical">
            <article
              className="tile is-child notification is-white"
              style={{
                border: '2px solid #d5d5d8',
                borderRadius: '25px',
                background: '#f5f5f5',
              }}
            >
              <p className="title is-size-5">Free Storage</p>
              <p className="subtitle mt-4">
                Unlimited storage for all yours images!
              </p>
            </article>
            <article
              className="tile is-child notification is-white"
              style={{
                boxShadow: 'none',
                border: '2px solid #d5d5d8',
                borderRadius: '25px',
                background: '#f5f5f5',
              }}
            >
              <p className="title is-size-5">Secured</p>
              <p className="subtitle mt-4">
                We use strong secured servers to manage all the event resources!
              </p>
            </article>
          </div>
          <div className="tile is-parent">
            <article
              className="tile is-child notification is-white"
              style={{
                boxShadow: 'none',
                border: '2px solid #d5d5d8',
                borderRadius: '25px',

                background: '#f5f5f5',
              }}
            >
              <p className="title is-size-5">Free Image Compressor</p>
              <p className="subtitle mt-4">
                All images are compressed without compromising on image quality!
              </p>
            </article>
          </div>
        </div>
        <div className="tile is-parent">
          <article
            className="tile is-child notification is-white"
            style={{
              boxShadow: 'none',
              border: '2px solid #d5d5d8',
              borderRadius: '25px',
              background: '#f5f5f5',
            }}
          >
            <p className="title is-size-5">Privacy</p>
            <p className="subtitle mt-4">
              We take privacy very serious. We do not track or sell information
              that are available to us!
            </p>
          </article>
        </div>
      </div>
      <div className="tile is-parent">
        <article
          className="tile is-child notification is-white"
          style={{
            boxShadow: 'none',
            border: '2px solid #d5d5d8',
            borderRadius: '25px',
            background: '#f5f5f5',
          }}
        >
          <div className="content">
            <p className="title is-size-5">Face Detection</p>
            <p className="subtitle mt-4">
              With our face detection technology, we create online gallery for
              users featuring them!
            </p>
          </div>
        </article>
      </div>
    </div>
  );
}
