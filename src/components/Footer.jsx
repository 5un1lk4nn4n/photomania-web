export default function Footer() {
  return (
    <footer className="footer has-background-grey-dark has-text-white-ter">
      <div className="content has-text-centered">
        <p>Copyright © picturetube.live</p>
      </div>
      <div className="columns">
        <div className="column content">
          <p
            className="bd-footer-link
            has-text-left"
          >
            Picturetube is web portal for photographers and event manager who wants
            to share event images instantly to the guests of the event.
          </p>
          <div className="block">
            <p>Big thanks for illustrations</p>
            <p>
              <a href="https://storyset.com" target="_blank" rel="noreferrer">
                All illustrations by Storyset
              </a>
            </p>
          </div>
        </div>

        <div className="column">
          <h4
            className="bd-footer-title
             has-text-weight-medium
             has-text-justify"
          >
            Explore
          </h4>

          <p className="bd-footer-link">
            <a href="/user-terms">
              <span className="icon-text">
                <span>User Agreement</span>
              </span>
            </a>
            <br />
            <a href="/privacy-policy">
              <span className="icon-text">
                <span>Privacy Policy</span>
              </span>
            </a>
          </p>
        </div>

        <div className="column">
          <h4
            className="bd-footer-title
             has-text-weight-medium
             has-text-justify"
          >
            Contact us
          </h4>

          <p className="bd-footer-link">
            <a href="mailto:contact@picturetube.live">
              <span className="icon-text">
                <span>contact@picturetube.live</span>
              </span>
            </a>
            <br />
            {/* <a href="https://wa.me/917305957605">
              <span className="icon-text">
                <span>Whatsapp</span>
              </span>
            </a>
            <br />
            <a href="tel:+917305957605">
              <span className="icon-text">
                <span>Call Us</span>
              </span>
            </a> */}
          </p>
        </div>
      </div>
    </footer>
  );
}
